import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);

		while (isNotFull(board) != false){
			int row,col;
			int counter = 0;
			do {
				System.out.print("Player 1 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				col = reader.nextInt();
				if (row <=3 && row > 0 && col <= 3 && col > 0 && board[row-1][col-1] ==  ' '){
					board[row - 1][col - 1] = 'X';
					printBoard(board);
					counter =0 ;
				}
				else if (row > 3 || row < 0 || col > 3 || col < 0 || board[row-1][col-1] !=  ' '){
					counter = 1;
				}

				if (isNotFull(board) == false && checkboard(board) != true){
					System.out.println("Draw");
					break;
				}

			}while( counter == 1);

			if (checkboard(board) ==true){
				System.out.println("Player 1 is winner.");
				break;
			}

			if (isNotFull(board) == false && checkboard(board) != true) {
				//System.out.println("Draw");
				break;
			}



			do {

				System.out.print("Player 2 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				col = reader.nextInt();
				if (row <=3 && row > 0 && col <= 3 && col > 0 && board[row-1][col-1] ==  ' '){
					board[row - 1][col - 1] = 'O';
					printBoard(board);
					counter = 0;
				}
				else if (row > 3 || row < 0 || col > 3 || col < 0 || board[row-1][col-1] !=  ' '){
					counter = 1;
				}

				if (isNotFull(board) == false && checkboard(board) != true){
					System.out.println("Draw");
					break;
				}

			}while( counter == 1 );

			if (checkboard(board) ==true){
				System.out.println("Player 2 is winner.");
				break;
			}
			if (isNotFull(board) == false && checkboard(board) != true){
				//System.out.println("Draw");
				break;
			}

		}
		reader.close();
	}

	public static boolean isNotFull(char [][] board){
		boolean flag = true;
		int count = 0;

		for (int row = 0; row < 3; row++){
			for (int column = 0 ; column < 3; column++){
				if (board[row][column] == ' '){
					count++;
					flag = true;
				}
				else if (count==0){
					flag = false;
				}
			}
		}
		return flag;
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

	public static boolean checkboard(char[][] board){
		if (checkRows(board)  || checkColumns(board) || checkDiagnol(board)){
			return true;
		}
		else
			return false;
	}

	public static boolean checkRows(char[][] board){
		for (int i = 0 ; i<3 ; i++){
			if ((board[i][0] == board[i][1] && board[i][0] == board[i][2]) && board[i][0] != ' '){
				return true;
			}
		}
		return false;
	}
	public static boolean checkColumns(char[][] board){
		for (int i = 0 ; i<3 ; i++){
			if ((board[0][i] == board[1][i] && board[0][i] == board[2][i]) && board[0][i] != ' '){
				return true;
			}
		}
		return false;
	}
	public static boolean checkDiagnol(char[][] board){
		if ((board[0][0] == board[1][1] && board[0][0] == board[2][2]) && board[0][0] != ' '){
			return true;
		}
		else if ((board[0][2] == board[1][1] && (board[0][2] == board[2][0])) && board[0][2] != ' '){
			return true;
		}
		else
			return false;
	}
}