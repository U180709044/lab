public class Point {

    int xCoord;
    int yCoord;

    public Point(int xCoord,int yCoord) {

        this.xCoord=xCoord;
        this.yCoord=yCoord;
    }

    public double distanceFromAPoint(Point p){
        return Math.sqrt(Math.pow((xCoord - p.xCoord),2)+Math.pow((yCoord-p.yCoord),2));
    }
}
