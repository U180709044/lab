package shapes2d;

import java.lang.Object;

public class Square extends Object{

    protected double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public String toString() {
        return "Square Side = " + side+
                ", Square Area = " + area();
    }

    public double area(){
        return Math.pow(side,2);
    }

}
