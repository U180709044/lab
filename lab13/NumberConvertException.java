public class NumberConvertException extends Exception {
    public NumberConvertException() {
        super("Not a valid number");
    }
}