import java.util.HashMap;
import java.util.Map;

public class StringToIntConverter {

    Map<String, Integer> map = new HashMap<>();

    public StringToIntConverter() {
        map.put("zero", 0);
        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);
        map.put("four", 4);
        map.put("five", 5);
        map.put("six", 6);
        map.put("seven", 7);
        map.put("eight", 8);
        map.put("nine", 9);
    }
    public Integer convert(String number) throws NumberConvertException{
        Integer value=this.map.get(number);
        if (value ==null){
            throw new NumberConvertException();
        }
        return value;
    }
}