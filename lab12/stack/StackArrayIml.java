package stack;

import java.util.ArrayList;

public class StackArrayIml implements Stack{
    private ArrayList<Object> stack = new ArrayList<Object>();
    @Override
    public void push(Object Item) {
        stack.add(0,Item);

    }

    @Override
    public Object pop() {
      return stack.remove(0);
    }

    @Override
    public boolean empty() {
        return stack.size()==0;
    }
}
